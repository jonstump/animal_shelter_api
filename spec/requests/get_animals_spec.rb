require 'rails_helper'

describe 'Get requests Animals' do

  let!(:animals) { FactoryBot.create_list(:animal, 10) }

  describe 'gets all animals', :type => :request do
    before { get animals_path }

    it 'returns all of the animals' do
      expect(JSON.parse(response.body).size).to eq(10)
    end

    it 'returns status code of 200' do
      expect(response).to have_http_status(:success)
    end
  end

  describe '#show', :type => :request do
    let!(:animal) { animals.first }

    context 'when successful' do
      before do
        get animal_path(animal)
        @parse = JSON.parse(response.body)
      end
      it 'returns a animal' do
        expect(@parse['id']).to eq animal.id
      end
      it { expect(response).to have_http_status :success }
    end

    context 'when unsuccessful' do
      before { get animal_path(animals.last.id + 1) }
      it { expect(response).to have_http_status :not_found }
    end
  end
end
