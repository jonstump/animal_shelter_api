require 'rails_helper'

describe 'Delete an animal', :type => :request do
  let!(:animal) { FactoryBot.create(:animal) }

  context 'when successful' do
    it 'deletes a animal' do
      delete animal_path(animal.id)
      expect(response).to have_http_status :no_content
    end
  end
  context 'when unsuccessful' do
    it 'return a 404' do
      delete animal_path(animal.id+1)
      expect(response).to have_http_status :not_found
    end
  end
end
