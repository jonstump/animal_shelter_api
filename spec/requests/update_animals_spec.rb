require 'rails_helper'

describe 'update an animal', :type => :request do
  let!(:animal) { FactoryBot.create(:animal) }
  let!(:attr) { { name: 'Eli', species: 'monkey', size: 'monkey sized' } }

  context 'when successful' do
    before do
      patch animal_path(animal.id), params: attr
      @parse = JSON.parse(response.body)
    end
    it 'updates an animal' do
      attr.entries do |key, val|
        it { expect(@parse[key]).to eq val }
      end
    end
    it 'returns a value of 200' do
      expect(response).to have_http_status :created
    end
  end
  context 'when unsuccessful' do
    it 'return a value of 422' do
      patch animal_path(animal.id), params: { age: '' }
      expect(response).to have_http_status :unprocessable_entity
    end
  end
end
