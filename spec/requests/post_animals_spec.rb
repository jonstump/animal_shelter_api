require 'rails_helper'

describe 'post an animal', :type => :request do
  before do
    post '/animals', params: { name: 'Garfield',
                               species: 'cat',
                               size: 'chungus',
                               age: '43',
                               about: 'All about that lasagna.',
                               vaccinated: 'true',
                               adopted: 'false' }
  end

  it 'returns name' do
    expect(JSON.parse(response.body)['name']).to eq('Garfield')
  end

  it 'returns species' do
    expect(JSON.parse(response.body)['species']).to eq('cat')
  end

  it 'returns size' do
    expect(JSON.parse(response.body)['size']).to eq('chungus')
  end

  it 'returns age' do
    expect(JSON.parse(response.body)['age']).to eq(43)
  end

  it 'returns the about' do
    expect(JSON.parse(response.body)['about']).to eq('All about that lasagna.')
  end

  it 'returns if vaccinated' do
    expect(JSON.parse(response.body)['vaccinated']).to eq(true)
  end

  it 'returns if adopted' do
    expect(JSON.parse(response.body)['adopted']).to eq(false)
  end

  it 'returns a created status' do
    expect(response).to have_http_status(:created)
  end
end
