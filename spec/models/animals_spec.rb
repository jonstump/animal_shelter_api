require 'rails_helper'

describe Animal do
  describe 'validation' do
    %i[name species size age].each { |property| it { should validate_presence_of property } }
  end
end
