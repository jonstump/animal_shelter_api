FactoryBot.define do
  factory(:animal) do
    name { Faker::Creature::Dog.name }
    species { Faker::Creature::Animal.name }
    size { Faker::Creature::Dog.size }
    age { Faker::Number.between(from: 1, to: 12) }
    about { Faker::Lorem.paragraph(sentence_count: 3) }
    vaccinated { Faker::Boolean.boolean }
    adopted { Faker::Boolean.boolean }
  end
end
