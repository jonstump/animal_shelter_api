class AnimalsController < ApplicationController
  swagger_controller :animals, 'Animal Management'

  swagger_api :index do
    summary 'Fetches all Animals'
    notes 'This lists all animals'
    param :path, :id, :integer, :required, 'Animal ID'
    response :ok, 'Success'
    response :not_acceptable, 'The request you made is not acceptable'
  end
  def index
    json_response(Animal.all)
  end

  swagger_api :create do
    summary 'Create a new Animal'
    param :form, :name, :string, :required, 'Name'
    param :form, :species, :string, :required, 'Species'
    param :form, :size, :string, :required, 'Size'
    param :form, :age, :integer, :required, 'Age'
    param :form, :about, :string, 'About'
    param :form, :vaccinated, :boolean, 'Vaccinated'
    param :form, :adopted, :boolean, 'Adopted'
    response :ok, 'Success'
    response :not_acceptable
  end
  def create
    animal = Animal.create!(animal_params)
    json_response(animal, :created)
  end

  swagger_api :show do
    summary 'Fetches a single Animal'
    param :path, :id, :integer, :optional, 'Animal ID'
    response :ok, 'Success', :Animal
    response :not_acceptable
    response :not_found
  end
  def show
    animal = Animal.find(params[:id])
    json_response(animal)
  end

  swagger_api :update do
    summary 'Updates an existing animal'
    param :path, :id, :integer, :required, 'Animal ID'
    param :form, :name, :string, :required, 'Name'
    param :form, :species, :string, :required, 'Species'
    param :form, :size, :string, :required, 'Size'
    param :form, :age, :integer, :required, 'Age'
    param :form, :about, :string, 'About'
    param :form, :vaccinated, :boolean, 'Vaccinated'
    param :form, :adopted, :boolean, 'Adopted'
    response :ok, 'Success'
    response :not_found
    response :not_acceptable
  end
  def update
    animal = Animal.find(params[:id])
    animal.update!(animal_params)
    json_response(animal, :created)
  end

  swagger_api :destroy do
    summary 'Deletes an existing Animal'
    param :path, :id, :integer, :optional, 'Animal ID'
    response :ok, 'Success'
    response :not_found
  end
  def destroy
    Animal.find(params[:id]).destroy!
  end

  private

  def animal_params
    params.permit(:name, :species, :size, :age, :about, :vaccinated, :adopted)
  end
end
