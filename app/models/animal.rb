# model for the Animals table. About intentionally left out in case it would need to be filled out after the animal has come to the shelter and there isn't info on them yet.
class Animal < ApplicationRecord
  validates :name, :species, :size, :age, {
    presence: true
  }
end
