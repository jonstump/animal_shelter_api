class Seed
  def self.begin
    seed = Seed.new
    seed.generate_animals
  end

  def generate_animals
    boolean = [true, false]
    100.times do |i|
      animal = Animal.create!(name: Faker::Creature::Dog.name,
                              species: Faker::Creature::Animal.name,
                              size: Faker::Creature::Dog.size,
                              age: Faker::Number.between(from: 1, to: 12),
                              about: Faker::Lorem.paragraph(sentence_count: 4),
                              vaccinated: boolean.sample,
                              adopted: boolean.sample)
    end
  end
end
Seed.begin
p "Created #{Animal.count} Animals."
