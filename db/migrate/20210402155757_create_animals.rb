# Class that creates the table to hold pets for the animal shelter
class CreateAnimals < ActiveRecord::Migration[5.2]
  def change
    create_table :animals do |t|
      t.string :name
      t.string :species
      t.string :size
      t.integer :age
      t.string :about
      t.boolean :vaccinated
      t.boolean :adopted

      t.timestamps
    end
  end
end
