# Ruby on Rails Building and API Project for Epicodus: Animal Shelter API

#### By Jon Stump

<img align="center" src="https://avatars2.githubusercontent.com/u/59323850?s=460&u=372c7d529b7379408ae54491ab3449b6e2f4d94d&v=4">

* * *

## Description

This is an API for a fictional animal shelter to show knowledge and understanding of creating an API with Ruby on Rails.

![Table Schema](/public/img/animals&#32;table.png)

* * *

## Technologies used

* Ruby
* Rspec
* Pry
* Rails
* Neovim
* Postgres
* Bundler
* FactoryBot
* git
* Rubocop
* Faker
* C8H10N4O2

* * *

## Documentation

| Action  | Route | Description |
| ------------- |:-------------:| -----:|
| Get | /animals | Fetches all Animals |
| Post | /animals | Creates a new Animal |
| Delete | /animals/id | Deletes an existing Animal |
| Get | /animals/id | Fetches a single Animal |
| Patch | /animals/id | Updates an existing Animal |

\
Since this project uses Swagger Docs you can also view this documentation when you load http://localhost:3000 on your own machine after following the instructions for installation below. It will also let you test the endpoints.

When testing with postman everything should work with the link http://localhost:3000/animals

* * *

## Installation

* Go to ( https://github.com/jonstump/animal_shelter_api ).

* Navigate to the code button on the github website.

* Click on the code button to open the menu.

- Copy the HTTPS code by clicking the clipboard next to the link.

- Within your Bash terminal navigate to your desired location by using cd fallowed by your desired directory.

```bash
 cd Desktop
```

- Once you have chosen your desired directory use the command.

```bash
git clone https://github.com/jonstump/animal_shelter_api
```

<div
  style="
    background-color: #d1ecf1;
    color: grey; padding: 6px;
    font-size: 9px;
    border-radius: 5px;
    border: 1px solid #d4ecf1;
    margin-bottom: 12px"
>
  <span
    style="
      font-size: 12px;
      font-weight: 600;
      color: #0c5460;"
  >
    ⓘ
  </span>
  <span
    style="
      font-size: 12px;
      font-weight: 900;
      color: #0c5460;
      margin-bottom: 24px"
  >
    Note :
  </span>
  If you have any problems make sure your HTTPS code is correct! The example above might not be the most recent HTTPS code!
</div>

* Now that the project is downloaded you will need to run the command 'bundle install' to install all of the gems from the Gemfile.

``` bash
bundle install
```

* In oder to run the tests enter 'rspec' into your terminal.

``` bash
rspec
```

* If you would like to see the code use the command "editor ." (where editor is your code editor. example code for VScode and vim for Vim) to open the project in your code editor.

``` bash
nvim .
```

* You will need to use rails in order to setup the databases for the project along with Postgres installed on your machine as well. To install the databases and tables run the following command in your terminal:

``` bash
rake db:setup
```

* To launch a local version of the site in your browser enter the following:

``` bash
rails s
```

This will launch a local host via Rails. You should be able to navigate to http://localhost:3000/ to see the Swagger Docs locally.

## To dos

* Add a search to allow users to search data points in the API.
* Add a random get feature


## Bugs

* Get on Swagger docs won't pull the data that is in the database.

## Resources

* [Swagger Docs Sitepoint documentation](https://www.sitepoint.com/do-the-right-thing-and-document-your-rails-api-with-swagger/)
* [Stackoverflow](https://stackoverflow.com)
* [Swagger Docs](https://github.com/richhollis/swagger-docs#inheriting-from-a-custom-api-controller)
* [Epicodus Learn to Program Ruby Path](https://www.learnhowtoprogram.com/ruby-and-rails)
* [Faker Documentation](https://github.com/faker-ruby/faker)
* [SQL Designer](https://ondras.zarovi.cz/sql/demo/)

* * *

## License

> [GPLv3](/LICENSE)\
> Jon Stump &copy; 2021

* * *

## Contact Information

_Jon Stump: [Email](jmstump@gmail.com)_
